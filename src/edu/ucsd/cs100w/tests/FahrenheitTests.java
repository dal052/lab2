package edu.ucsd.cs100w.tests;

import junit.framework.TestCase;
import edu.ucsd.cs110w.temperature.Celsius;
import edu.ucsd.cs110w.temperature.Fahrenheit;
import edu.ucsd.cs110w.temperature.Temperature;

public class FahrenheitTests extends TestCase {
	private float delta = 0.001f;
	public void testFahrenheit(){
		float value = 12.34f;
		Fahrenheit temp = new Fahrenheit(value);
		assertEquals(value, temp.getValue(). delta);
	}
	public void testFahrenheitToString(){
		float value = 12.34f;
		Fahrenheit temp = new Fahrenheit(value);
		String string = temp.toString();
	}
}
