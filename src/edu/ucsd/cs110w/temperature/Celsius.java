/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wba
 *
 */
public class Celsius extends Temperature {
	public Celsius (float t)
	{
		super(t);
	}
	public String toString()
	{
		//Complete this method
		return "" + getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius(getValue());
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit(getValue() * 9.0f / 5.0f + 32);
	}
	public Temperature toKelvin() {
		return new Kelvin(getValue() + 273.15f);
	}
}
