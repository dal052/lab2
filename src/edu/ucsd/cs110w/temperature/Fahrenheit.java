/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wba
 *
 */
public class Fahrenheit extends Temperature {
	public Fahrenheit (float t)
	{
		super(t);
	}
	public String toString()
	{
		//Complete this method
		return "" + getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius((getValue()-32) * 5.0f / 9.0f);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit(getValue());
	}
	public Temperature toKelvin() {
		return new Kelvin(5.0f / 9.0f * (getValue() - 32) + 273.15f);
	}
}
