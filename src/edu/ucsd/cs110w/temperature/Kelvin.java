/**
 * 
 */
package edu.ucsd.cs110w.temperature;
/**
 * TODO (cs110wba): write class javadoc
 *
 * @author cs110wba
 *
 */
public class Kelvin extends Temperature {
	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return "" + getValue() + " K";
	}
	@Override
	public Temperature toCelsius() {		// TODO: Complete this method
		return new Celsius(getValue() - 273.15f);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO: Complete this method
		return new Fahrenheit(getValue() * 9.0f / 5.0f -459.67f);
	}
	@Override
	public Temperature toKelvin(){
		return new Kelvin(getValue());
	}
}


