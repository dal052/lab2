package edu.ucsd.cs110w.tests;

import junit.framework.TestCase;
import edu.ucsd.cs110w.temperature.Celsius;
import edu.ucsd.cs110w.temperature.Fahrenheit;
import edu.ucsd.cs110w.temperature.Temperature;

public class FahrenheitTests extends TestCase {
	private float delta = 0.001f;
	public void testFahrenheit(){
		float value = 12.34f;
		Fahrenheit temp = new Fahrenheit(value);
		assertEquals(value, temp.getValue(), delta);
	}
	public void testFahrenheitToString(){
		float value = 12.34f;
		Fahrenheit temp = new Fahrenheit(value);
		String string = temp.toString();
	}
	public void testFahrenheitToKelvin()
	{
	Fahrenheit temp = new Fahrenheit(32);
	Temperature convert = temp.toKelvin();
	assertEquals(273.15f, convert.getValue(), delta);
	temp = new Fahrenheit(212);
	convert = temp.toKelvin();
	assertEquals(373.15f, convert.getValue(), delta);	
	}
}
